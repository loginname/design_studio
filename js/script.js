$(function() {
	$('.toggles button').click(function() {
		var getid = this.id;
		var getcurrent = $('.posts .' + getid);
		
		getcurrent.show(500);
		$('.post').not(getcurrent).hide(500);
	});

	$('#showall').click(function() {
		$('.post').show(500);
	});
});

$(document).ready(function(){
	$(".owl-carousel").owlCarousel({ 
		items:8 
	});
});

if($(window).width() < 860) {
	$(".owl-carousel").owlCarousel({ 
		items:4
	});
}